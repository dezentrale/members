import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.openapitools.generator.gradle.plugin.tasks.GenerateTask
import org.openapitools.generator.gradle.plugin.tasks.ValidateTask

group = "space.dezentrale"
version = "0.0.1"
java.sourceCompatibility = JavaVersion.VERSION_17

repositories {
    mavenCentral()
    maven { url = uri("https://repo.spring.io/milestone") }
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "17"
}

plugins {
    val kotlinVersion = "1.7.21"
    kotlin("jvm") version kotlinVersion
    id("org.jetbrains.kotlin.plugin.jpa") version "1.7.21"
    kotlin("plugin.spring") version kotlinVersion
    id("org.springframework.boot") version "3.0.0"
    id("io.spring.dependency-management") version "1.1.0"
    id("org.openapi.generator") version "6.2.1"
}

dependencies {
    val kotlinxCoroutinesVersion = "1.6.1"
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$kotlinxCoroutinesVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor:$kotlinxCoroutinesVersion")
    implementation("org.springdoc:springdoc-openapi-webflux-ui:1.6.13")
    implementation("com.google.code.findbugs:jsr305:3.0.2")

    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-yaml")
    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-xml")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
    implementation("org.jetbrains.kotlin:kotlin-reflect")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("io.projectreactor:reactor-test")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit5")
    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(module = "junit")
    }
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:$kotlinxCoroutinesVersion")
    runtimeOnly("ch.qos.logback:logback-classic")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "17"
    }
}
val inputSpec: String by project

tasks.register("validateMembersApi", ValidateTask::class)

tasks.register("generateMembersApi", GenerateTask::class) {
    dependsOn("validateMembersApi")
}

openApiValidate {
    inputSpec.set("$rootDir/src/main/resources/META-INF/openapi/members.yml")
}

openApiGenerate {
    groupId.set("space.dezentrale")
    generatorName.set("kotlin-spring")
    inputSpec.set("$rootDir/src/main/resources/META-INF/openapi/members.yml")
    apiPackage.set("space.dezentrale.members.api")
    modelPackage.set("space.dezentrale.members.model")
    outputDir.set("$buildDir/generated/kotlin")

    configOptions.set(
        mapOf(
            "artifactId" to "dezentrale-members",
            "artifactVersion" to "0.0.1",
            "interfaceOnly" to "true",
            "sourceFolder" to "",
            "reactive" to "true"
        )
    )
}

sourceSets.main {
    java.srcDirs("src/main/kotlin", "$buildDir/generated/kotlin")
}

tasks.withType<Test> {
    useJUnitPlatform()
}
