package space.dezentrale.members.models

import com.fasterxml.jackson.annotation.JsonProperty
import io.swagger.v3.oas.annotations.media.Schema

/**
 * 
 * @param status The HTTP code which led to this error
 * @param title A short title of the problem in kebab-case
 * @param description A description of the error and possible suggestions for solutions
 */
data class Error(

    @Schema(example = "null", description = "The HTTP code which led to this error")
    @field:JsonProperty("status") val status: Int? = null,

    @Schema(example = "not-found", description = "A short title of the problem in kebab-case")
    @field:JsonProperty("title") val title: String? = null,

    @Schema(example = "A member with the email you are looking for does not exist. Perhaps you have made a mistake?", description = "A description of the error and possible suggestions for solutions")
    @field:JsonProperty("description") val description: String? = null
) {

}

