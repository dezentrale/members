package space.dezentrale.members.models

import com.fasterxml.jackson.annotation.JsonProperty
import io.swagger.v3.oas.annotations.media.Schema
import jakarta.validation.Valid
import jakarta.validation.constraints.Min
import jakarta.validation.constraints.Pattern

/**
 * The member class represents every paying member in the hack space dezentrale.
 * @param nickname The self-chosen nickname of every haeckse or hacker in the dezentrale. This must be unique!
 * @param email The email address of every haeckse or hacker in the dezentrale, under which she or he should also be reachable.
 * @param dateOfBirth The member's date of birth. There may be legal matters to consider if the member is a  minor.
 * @param number The member number, which (in the past) was and is simply incremented. This number has no identifying effect and should no longer be used. It will continue to be used solely for historical reasons.
 * @param id The UUID to identify the member in various processes.
 * @param pgpKey The member's PGP key if they want encrypted communication.
 * @param type 
 * @param role 
 * @param status 
 */
data class Member(

    @Schema(example = "wau", required = true, description = "The self-chosen nickname of every haeckse or hacker in the dezentrale. This must be unique!")
    @field:JsonProperty("nickname", required = true) val nickname: String,

    @get:Pattern(regexp="(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])")
    @Schema(example = "wau.holland@ccc.de", required = true, description = "The email address of every haeckse or hacker in the dezentrale, under which she or he should also be reachable.")
    @field:JsonProperty("email", required = true) val email: String,

    @field:Valid
    @Schema(example = "Thu Dec 20 01:00:00 CET 1951", required = true, description = "The member's date of birth. There may be legal matters to consider if the member is a  minor.")
    @field:JsonProperty("dateOfBirth", required = true) val dateOfBirth: java.time.LocalDate,

    @get:Min(1)
    @Schema(example = "1", description = "The member number, which (in the past) was and is simply incremented. This number has no identifying effect and should no longer be used. It will continue to be used solely for historical reasons.")
    @field:JsonProperty("number") val number: Int? = null,

    @get:Pattern(regexp="^[0-9a-fA-F]{8}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{12}$")
    @Schema(example = "f2012985-fc7a-4500-a689-bf7637bebcfe", description = "The UUID to identify the member in various processes.")
    @field:JsonProperty("id") val id: String? = null,

    @Schema(example = "-----BEGIN PGP SIGNATURE----- iQIzBAABCAAdFiEEqcXfTSLpmZjZh1pREMAcWi9gWecFAmLP1OIACgkQEMAcWi9g WefaaRAA8OO6+VR30iCtEygf3HoIn85CBBYDjskWr+XNQ81dGVJjyTTl+MsRS+AY KV2aNn3ID9uufQGj6Jzf82EYHb401EZU0CH1gkQzAp5PN5bzwpbFwJhbuluJToaB Q7Kg94SQEsjwNkEEw6INZr/OQT+HW1YiuNKIhLncf/ErHMq8EqT3lkHxjQYRb6mt hpyP4/4esXNXOKVDxfThGXZFXZ8m9JQff5hglEYzz8+tnkc/XyxwNd7tqc1/FbKE U+84kVyPEHcUG5OMqN000CNpQmjfOTcUbdN44lYAL5WGe1Z7qTg4jf66LqmOoP9R ePzP3DsV5rBnKkSdoxryfBjvUW/4E5KlBMfgtHlGgVGQv/H0qbgBpHJEB08M4xGh GnSovlh8KFMSBaQHEaqux9Bv5yAQjmd5X5u+sES9uDETRuQ0wmcdXhOoFZW4NtiM jiq6wH+nhXa9oLV4G7simuCNpEtT038CCzUpla1tlnnxthimBE73uxy40z3cjtPw uoKropdjqgiKy4YCckKpA+hAY+/4pMRQ+V+ZgMqZmnU6Exkf1lJNwsJd2FK0u+Hu ZULkFS2Y2QFvATd+0dNNLvK+itYUaYdemOuYmjV0gbIvFzR9cqRNN6W0Dc0bpE+U kK+tiYuLhmpiRxwVkTgOZiaIjmWFLx1YrOSN7O2P3BFymq5QVNA= =tjq+ -----END PGP SIGNATURE-----", description = "The member's PGP key if they want encrypted communication.")
    @field:JsonProperty("pgpKey") val pgpKey: String? = null,

    @field:Valid
    @Schema(example = "null", description = "")
    @field:JsonProperty("type") val type: MemberType? = null,

    @field:Valid
    @Schema(example = "null", description = "")
    @field:JsonProperty("role") val role: MemberRole? = null,

    @field:Valid
    @Schema(example = "null", description = "")
    @field:JsonProperty("status") val status: MemberStatus? = null
) {

}

