package space.dezentrale.members.models

import com.fasterxml.jackson.annotation.JsonProperty

/**
* There are several roles that are newly determined every two years through the board  elections. If a member has one of these roles, he/she has extended possibly extended rights within this application.
* Values: NORMAL,CHAIRMAN,TREASURER,SECRETARY,ASSESSOR
*/
enum class MemberRole(val value: String) {

    @JsonProperty("NORMAL") NORMAL("NORMAL"),
    @JsonProperty("CHAIRMAN") CHAIRMAN("CHAIRMAN"),
    @JsonProperty("TREASURER") TREASURER("TREASURER"),
    @JsonProperty("SECRETARY") SECRETARY("SECRETARY"),
    @JsonProperty("ASSESSOR") ASSESSOR("ASSESSOR")
}

