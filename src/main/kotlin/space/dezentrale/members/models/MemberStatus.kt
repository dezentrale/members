package space.dezentrale.members.models

import com.fasterxml.jackson.annotation.JsonProperty

/**
* This status expresses the extent to which a member has already been accepted and processed. The status \"DELETED\" means that the data is still present only for legal reasons, but is to be permanently deleted at the next opportunity.
* Values: UNINITIALIZED,GREETED,ACTIVE,BANNED,DISABLED,DELETED
*/
enum class MemberStatus(val value: String) {

    @JsonProperty("UNINITIALIZED") UNINITIALIZED("UNINITIALIZED"),
    @JsonProperty("GREETED") GREETED("GREETED"),
    @JsonProperty("ACTIVE") ACTIVE("ACTIVE"),
    @JsonProperty("BANNED") BANNED("BANNED"),
    @JsonProperty("DISABLED") DISABLED("DISABLED"),
    @JsonProperty("DELETED") DELETED("DELETED")
}

