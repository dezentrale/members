package space.dezentrale.members.models

import com.fasterxml.jackson.annotation.JsonProperty

/**
* The main reason for membership is that we can pay the rent, electricity and internet in our premises. However, there are members who cannot be present so often and therefore prefer to be only a supporting member.
* Values: REGULAR,SUPPORTING
*/
enum class MemberType(val value: String) {

    @JsonProperty("REGULAR") REGULAR("REGULAR"),
    @JsonProperty("SUPPORTING") SUPPORTING("SUPPORTING")
}

